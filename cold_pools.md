# Cold pools

## Introduction

In atmospheric science, a cold pool (CP) is an evaporatively-driven downdraft of unsaturated air cooled by evaporation of precipitation. Cold pools can originate from regimes that range from thunderstorm clouds to precipitating shallow clouds, and they are ubiquitous both over land and ocean.

The characteristics and impact of cold pools vary depending on the properties of the parent convection, namely its rain rates, and the large-scale environment in which they originate (Zuidema et al., 2017). Cold pools can have a strong impact on cloud cover and organization, by triggering new convection at the gust front and suppressing clouds in its interior (Zuidema et al., 2017).

Cold pools can be detected and studied using observations and high resolution numerical simulations (Zuidema et al., 2017).


## Characteristics

### Cold pool properties, formation, and recovery 

Cold pools consist of a large-scale mass of cold air (Glickman, 2000) surrounded by warmer air (according to the American Meteorological society or AMS). Over the ocean, these masses of cold surface air are mostly caused by cooling through evaporation of precipitation from shallow and thunderstorm clouds in unsaturated air. Evaporation of liquid precipitation removes energy from the air to cool it and moisten it, making it denser than the environmental air. In addition, the falling rain drags the air around it. These effects cause the air mass to sink towards the surface, leading to a rapid decrease in surface air temperature and generating a divergent flow that moves radially away from the precipitation (Kirsch et al., 2022). As the density current spreads horizontally, dry and cold air is injected into the boundary layer due to the penetrative downdrafts, causing the central area of the cold pool, sometimes referred to as the cold pool wake, to be drier than the edges. The edge or boundary of the outward-spreading density current is the cold pool gust front, which can trigger new convection through the combination of its enhanced moisture and the mechanical lifting of surrounding air (Tompkins, 2001, Feng et al., 2015, Torri et al., 2015, Drager et al., 2020). The cold pool gust front can therefore usually be identified as a mesoscale cloud arc (Vogel et al., 2021). 

Cold pools span 10-200 km in diameter, and spread at the same rate as the advection of the cloud layer (Wilbanks et al., 2015). They end when their features can no longer be distinguished from the large-scale flow, or when their signature on meteorological variables is within the background variability of the environment. The recovery in temperature usually occurs faster in the interior of cold pools, when air from above the boundary layer is entrained into the cold pool wake (Tompkins, 2001).


### Cold pool origins

Nearly all shallow clouds in the trade-wind region that produce precipitation rates larger than 1 mm/h are associated with cold pools (Zuidema et al., 2012). Cold pools are ubiquitous both over land and ocean (Zipser, 1977). 

Cold pool characteristics differ depending on the depth of the parent convection (deep or shallow), and therefore on the environment in, which they form. The properties of cold pools formed in the trades (characterized by shallow convection) were observed to vary significantly from properties of cold pools formed from tropical deep convection (Zuidema et al., 2012). For example, they are associated with temperature drops that are on average 2 K weaker, and they experience less drying and smaller wind speed enhancement. The trade wind region is mainly drier and is characterized by subsiding motion that caps the growth of convetion and maintains clouds shallow. In addition, precipitation-driven downdrafts at different levels in regions of shallow convection can lead to complex gradients in the thermodynamic profiles (Zuidema et al., 2012, Vogel et al., 2021).


## Impacts

### Triggering convection

Cold pools are found to trigger secondary convection at their edges, due to the combination of mechanical lifting and enhanced water vapor in the gust front (Tompkins et al. 2001, Zuidema et al. 2017, Kirsch et al. 2022). In regions of deep convection over the ocean, with low vertical shear, cold pools are found to trigger new convection thermodynamically (Tompkins et al. 2001). The cold pools spread to a radius at which the velocities are small, and thus the ability to mechanically lift air is also small. At this stage, however, the water vapor and equivalent potential temperature (and thus the CAPE), are high at the gust front, leading to the generation of new convection at the edges or gust front of the cold pool (Tompkins, 2001). Multiple colliding cold pools are more likely to trigger new convection (Feng et al. 2015, Haerter et al. 2019).

Over land, cold pools also tend to trigger convection. Here the effect of aerosols and surface fluxes are of high importance (Schlemmer and Hohenegger, 2016).

### Convective and cloud organization

It has been shown that specific mesoscale cloud organization patterns (Stevens et al., 2015) in the trade-wind region has a strong impact on the occurrence and properties of cold pools (Vogel et al., 2021). This is due to the different environmental conditions, as well as rain and cloud properties, associated with different patterns (Stevens et al. 2020, Schultz et al. 2021). In the tropics, cold pools may aid in the transition from shallow to deep convection. It is not yet clear how important cold pools are for maintaining or initiating patterns.

Another important role of cold pools can be found in the evolution of convective aggregation (Haerter et al. 2019). Cold pools tend to homogenize the convection and moisture fields through the divergence of near-surface air, and therefore act to oppose convective self-aggregation (Jeevanjee and Romps, 2013). This is the case for deep (Muller and Bony, 2015) and shallow convection (Narenpitak et al. 2023). Cold pools have been pointed to as the source of the domain-dependence of cloud resolving models (CRM) to initiate convective self-aggregation. Convective self-aggregation can only occur at domains larger than ~200km. In larger domains, cold pools would  dissipate before they could travel far enough to inhibit aggregation of convection. There is a competition between the homogenizing effect of cold pools and the inflow of moisture from the dry regions into a convectively aggregated cell, which is thought to contribute to edge-intensified convection (Windmiller and Hohenegger, 2019).

## Cloud cover

Cold pools are expected to hsve sn influence on the cloud cover through thermodynamic and dynamic effects. However, this is still an open topic of research (Zuidema et al. 2017, Vogel et al. 2021). In the trade wind region, this is especially important because the cloud cover greatly contributes to the planetary albedo. Challenges in representing and observing microphysical processes, wind shear effects, and the recovery of cold pools, hinder the understanding of the relationship between cold pools and cloud cover (Zuidema et al., 2017).


## Observations

From satellite images, cold pools can be identified as mesoscale arcs of clouds surrounding clear-sky areas or stratiform decks (Vogel et al., 2021). Common detection methods rely on measurements of strong and abrupt surface temperature drops (de Szoeke et al. 2017, Kirsch et al. 2021, Vogel et al. 2021, Kruse et al. 2022) or the onset of strong rain rates (Young et al. 1995). 

Common detection methods rely on measurements of strong and abrupt surface temperature drops (de Szoeke et al. 2017, Kirsch et al. 2021, Vogel et al. 2021, Kruse et al. 2022) or the onset of strong surface rain rates (Young et al., 1995), although not all cold pools are associated with strong rain rates at the surface (Zuidema et al., 2012). Cold pools can also be identified from changes in depth of the atmospheric mixed layer (Touze-Peiffer et al., 2021), or from synthetic aperture radar images (Brioulet et al. 2023).

Cold pools have been studied from observations taken during several field campaigns. For example during the Rain in Cumulus over the Ocean campaign
(RICO; Rauber et al. 2007) in the eastern Caribbean between December 2004 and January 2005, during the Dynamics of the Madden–Julian Oscillation experiment
(DYNAMO; Yoneyama et al. 2013) in the Indian Ocean, from the Barbados Cloud Observatory over 12 years (Vogel et al., 2021, BCO link) and during EUREC4A (Touze-Peiffer et al., 2021, EUREC4A link), and from a dense station network in Hamburg, Germany (Kirsch et al., 2022) during the FESSTVAL campaign (FESSTVAL link, UHH weathermast link), to name some examples.


## Modeling

Cold pools have been studied using Large Eddy Simulations (LES) (Seifert et al. 2013, Hirt et al. 2020) and Cloud Resolving Models (CRM) over smaller domains (Tompkins et al. 2001, Chandra et al. 2018). Models reproduce the water vapor rings seen in observations, but may overestimate the moisture content of these rings (Feng et al. 2015, Zuidema et al. 2017, Chandra et al. 2018). Difficulties in modeling cold pools arise in the representation of turbulent mixing and microphysics, which occur at the very small scales. The boundary conditions at the edges of the model domain must also be considered and impact the properties of simulated cold pools (Li et al. 2014, 2015).


